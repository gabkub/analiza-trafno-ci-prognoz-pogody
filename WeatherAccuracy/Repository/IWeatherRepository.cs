﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using WeatherAccuracy.Models;

namespace WeatherAccuracy.DAL
{
    public interface IWeatherRepository
    {
        IEnumerable<WeatherModel> GetAll();
        WeatherModel Get(ObjectId id);
        void Update(WeatherModel model);
        void Add(WeatherModel model);
        void Delete(ObjectId id);
        long GetWeatherDataCount();
    }
}
