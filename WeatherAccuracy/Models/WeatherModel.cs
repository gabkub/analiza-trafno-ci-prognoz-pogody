﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;

namespace WeatherAccuracy.Models
{
    [BsonIgnoreExtraElements]
    public class WeatherModel
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string DataPobrania { get; set; }
        public string GodzinaPobrania { get; set; }
        public Observed AktualnaPogoda { get; set; }
        public Forecast Pogoda5dni { get; set; }
    }

    public class Observed : Dictionary<string, WeatherData> { }
    public class Forecast : Dictionary<string, Dictionary<string, WeatherData[]>> { }

    public class WeatherData
    {
        public double? Timestamp { get; set; }
        public double Temperatura { get; set; }
        public double Cisnienie { get; set; }
        public double PredkoscWiatru { get; set; }
        public double Wilgotnosc { get; set; }
        public double SumaOpadu { get; set; }
        public double? Zachmurzenie { get; set; }
    }
}
