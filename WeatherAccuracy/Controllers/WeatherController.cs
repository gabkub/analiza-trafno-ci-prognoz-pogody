﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using CsvHelper;
//using System.Windows;
using Newtonsoft.Json;
using WeatherAccuracy.DAL;
using WeatherAccuracy.Models;
using Expression = System.Linq.Expressions.Expression;

namespace WeatherAccuracy.Controllers
{
    public class WeatherController : Controller
    {
        private string[] _apis =
        {
            "OpenWeather",
            "Weatherbit",
            "DarkSky",
            "VisualCrossing",
            "ClimaCell"
        };

        private string[] _cities =
        {
            "Krakow",
            "Wroclaw",
            "Warszawa",
            "Lublin",
            "Gdansk",
            "Szczecin"
        };

        private IList<int> _missingIndexes;
        private IList<DateTime> _observedDates;

        private string[] _parameters =
        {
            "Cisnienie",
            "PredkoscWiatru",
            "SumaOpadu",
            "Temperatura",
            "Wilgotnosc"
        };

        private int _weatherDataCount;
        private IList<WeatherModel> _weatherDataList;
        private IWeatherRepository _weatherRepository;

        public WeatherController()
        {
            _weatherRepository = new WeatherRepository(new MongoContext());
            _observedDates = new List<DateTime>();
            _weatherDataCount = (int)_weatherRepository.GetWeatherDataCount();
        }

        public WeatherController(IWeatherRepository weatherRepository)
        {
            _weatherRepository = weatherRepository;
        }

        public ActionResult Ranking()
        {
            Array.Sort(_cities);
            ViewData["cityOptions"] = _cities;
            ViewData["parameters"] = _parameters;
            return View();
        }

        [HttpGet]
        public JsonResult GetAccuracyDataJson(string cityName, string[] weatherParams)
        {
            if (weatherParams == null) weatherParams = _parameters;
            CheckAndGetData();
            var observations = cityName != "All"
                ? GetObservedWeatherByCity(1, _weatherDataCount, cityName)
                : GetObservedWeather(1, _weatherDataCount);

            var accuracies = new Dictionary<string, double>();

            foreach (var api in _apis)
            {
                var forecast = cityName != "All"
                    ? GetForecastWeatherForApiByCity(api, 0, _weatherDataCount, cityName)
                    : GetForecastWeatherForApi(api, 0, _weatherDataCount);
                accuracies.Add(api, (1 - CalculateUForApi(observations, forecast, weatherParams)) * 100);
            }

            var accDesc = accuracies.OrderByDescending(pair => pair.Value)
                .ToDictionary(pair => pair.Key, pair => pair.Value);

            return Json(JsonConvert.SerializeObject(accDesc), JsonRequestBehavior.AllowGet);
        }

        public List<int> GetMissingDatesIndexes()
        {
            var indexes = new List<int>();
            FillObservationDates();

            for (var i = 0; i < _observedDates.Count - 1; i++)
                if (_observedDates[i].AddDays(1) != _observedDates[i + 1])
                    indexes.Add(i);
            indexes.Sort();
            return indexes;
        }

        public void FillObservationDates()
        {
            for (var i = 1; i < _weatherDataCount; i++)
                _observedDates.Add(DateTime.Parse(_weatherDataList[i].DataPobrania));
        }

        public IEnumerable<WeatherData> GetObservedWeather(int indexStart, int indexEnd)
        {
            var observed = new List<Observed>();
            for (var i = indexStart; i < indexEnd; i++) observed.Add(_weatherDataList[i].AktualnaPogoda);
            var observedFlat = observed.SelectMany(x => x.Values).ToList();
            return observedFlat;
        }

        public IEnumerable<WeatherData> GetObservedWeatherByCity(int indexStart, int indexEnd,
            string cityName = "Krakow")
        {
            var observed = new List<WeatherData>();
            for (var i = indexStart; i < indexEnd; i++) observed.Add(_weatherDataList[i].AktualnaPogoda[cityName]);
//            var observedFlat = observed.SelectMany(x => x.Values).ToList();
            return observed;
        }

        public IEnumerable<WeatherData> GetForecastWeatherForApi(string apiName, int indexStart, int indexEnd)
        {
            var apiForecastList = new List<List<WeatherData>>();

            for (var i = indexStart; i < indexEnd; i += 5)
            {
                var forecastToAdd = new List<List<WeatherData>>();
                var forecastToAddFlatAndOrdered = new List<WeatherData>();
                var pogodaApi = _weatherDataList[i].Pogoda5dni.Where(x => x.Key == apiName).ToList();
                var apiForecast = pogodaApi.SelectMany(x => x.Value).ToList();
                var indexesToIgnore = GetIndexesToIgnore(i);

                foreach (var cityForecast in apiForecast)
                {
                    var cf = cityForecast.Value.Where((x, ind) => !indexesToIgnore.Contains(ind)).ToList();
                    forecastToAdd.Add(cf);
                }

                for (var j = 0; j < 5 - indexesToIgnore.Count; j++)
                    foreach (var f in forecastToAdd)
                        forecastToAddFlatAndOrdered.Add(f[j]);

                apiForecastList.Add(forecastToAddFlatAndOrdered);
            }

            var apiForecastFlat = apiForecastList.SelectMany(x => x).ToList();

            return apiForecastFlat;
        }

        public IEnumerable<WeatherData> GetForecastWeatherForApiByCity(string apiName, int indexStart, int indexEnd,
            string cityName = "Krakow")
        {
            var apiForecastList = new List<List<WeatherData>>();

            for (var i = indexStart; i < indexEnd; i += 5)
            {
                var forecastToAdd = new List<List<WeatherData>>();
                var forecastToAddFlatAndOrdered = new List<WeatherData>();

                var pogodaApi = _weatherDataList[i].Pogoda5dni.Where(x => x.Key == apiName).ToList();
                var apiForecast = pogodaApi.SelectMany(x => x.Value).Where(x => x.Key == cityName).ToList();
                var indexesToIgnore = GetIndexesToIgnore(i);

                foreach (var cityForecast in apiForecast)
                {
                    var cf = cityForecast.Value.Where((x, ind) => !indexesToIgnore.Contains(ind)).ToList();
                    forecastToAdd.Add(cf);
                }

                for (var j = 0; j < 5 - indexesToIgnore.Count; j++)
                    foreach (var f in forecastToAdd)
                        forecastToAddFlatAndOrdered.Add(f[j]);

                apiForecastList.Add(forecastToAddFlatAndOrdered);
            }

            var apiForecastFlat = apiForecastList.SelectMany(x => x).ToList();

            return apiForecastFlat;
        }


        public List<int> GetIndexesToIgnore(int ind)
        {
            var indexes = new List<int>();
            foreach (var missing in _missingIndexes)
                if (missing >= ind && missing < ind + 5)
                {
                    var index = Math.Abs(missing - ind);
                    indexes.Add(index);
                }

            return indexes;
        }

        public IEnumerable<double> GetParameter(IEnumerable<WeatherData> list, string weatherParam)
        {
            var param = Expression.Parameter(typeof(WeatherData), "x");
            var expression = Expression.Lambda<Func<WeatherData, double>>(
                Expression.Property(param, weatherParam),
                param
                ).Compile();

            var t = list.Select(expression).ToList();

            return t;
        }

        public double GetSumOfSquaredErrors(IEnumerable<WeatherData> observedWeather,
            IEnumerable<WeatherData> forecastWeather, string[] parameters)
        {
            double sum = 0;
            foreach (string weatherParameter in parameters)
            {
                sum += CalculateSumOfSquaredError(GetParameter(observedWeather, weatherParameter),
                    GetParameter(forecastWeather, weatherParameter));
            }
            return sum;
        }

        public double GetSumOfSquared(IEnumerable<WeatherData> weather, string[] parameters)
        {
            double sum = 0;

            foreach (string weatherParameter in parameters)
            {
                sum += CalculateSumOfSquared(GetParameter(weather, weatherParameter));
            }

            return sum;
        }

        public double CalculateUForApi(IEnumerable<WeatherData> observedWeather,
            IEnumerable<WeatherData> forecastWeather, string[] parameters)
        {
            var sumOfSquaredErrors = GetSumOfSquaredErrors(observedWeather, forecastWeather, parameters);
            var sumOfSquaredObservations = GetSumOfSquared(observedWeather, parameters);
            var sumOfSquaredForecasts = GetSumOfSquared(forecastWeather, parameters);
            var u = CalculateU(sumOfSquaredErrors, sumOfSquaredObservations, sumOfSquaredForecasts);
            return u;
        }

        public double CalculateSumOfSquaredError(IEnumerable<double> observedParameter,
            IEnumerable<double> forecastParameter)
        {
            double error = 0;
            for (var i = 0; i < observedParameter.Count(); i++)
                error += Math.Pow(observedParameter.ElementAt(i) - forecastParameter.ElementAt(i), 2);

            return error;
        }

        public double CalculateSumOfSquared(IEnumerable<double> list)
        {
            double sum = 0;
            foreach (var value in list) sum += Math.Pow(value, 2);
            return sum;
        }

        public double CalculateU(double a, double b, double c)
        {
            return Math.Sqrt(a) / (Math.Sqrt(b) + Math.Sqrt(c));
        }

        //        public double CalculateMapeForApi(IEnumerable<WeatherData> observedWeather,
        //            IEnumerable<WeatherData> forecastWeather)
        //        {
        //            var count = observedWeather.Count() * 5;
        //            var mape = GetMape(observedWeather, forecastWeather, count);
        //            return mape;
        //        }
        //
        //        public double GetMape(IEnumerable<WeatherData> observedWeather,
        //            IEnumerable<WeatherData> forecastWeather, int count)
        //        {
        //            var sum = CalculateMape(observedWeather.Select(x => x.Cisnienie).ToList(),
        //                forecastWeather.Select(x => x.Cisnienie).ToList(), count);
        //            sum += CalculateMape(observedWeather.Select(x => x.PredkoscWiatru).ToList(),
        //                forecastWeather.Select(x => x.PredkoscWiatru).ToList(), count);
        //            sum += CalculateMape(observedWeather.Select(x => x.SumaOpadu).ToList(),
        //                forecastWeather.Select(x => x.SumaOpadu).ToList(), count);
        //            sum += CalculateMape(observedWeather.Select(x => x.Temperatura).ToList(),
        //                forecastWeather.Select(x => x.Temperatura).ToList(), count);
        //            sum += CalculateMape(observedWeather.Select(x => x.Wilgotnosc).ToList(),
        //                forecastWeather.Select(x => x.Wilgotnosc).ToList(), count);
        //            return sum;
        //        }
        //
        //        public double CalculateMape(IEnumerable<double> observedParameter, IEnumerable<double> forecastParameter,
        //            int count)
        //        {
        //            double mape = 0;
        //
        //            for (int i = 0; i < observedParameter.Count(); i++)
        //            {
        //                var o = observedParameter.ElementAt(i);
        //                mape += Math.Abs((o - forecastParameter.ElementAt(i)) / o);
        //            }
        //
        //            mape /= count;
        //            return mape;
        //        }


        public ActionResult WeatherData()
        {
            ViewData["parameterOptions"] = _parameters;
            ViewData["cityOptions"] = _cities;
            return View();
        }

        [HttpGet]
        public JsonResult GetWeatherDataJson(string cityName = "Krakow")
        {
            CheckAndGetData();

            var observations = GetObservedWeatherByCity(1, _weatherDataCount, cityName);
            AppendToXlsFile($"D:/Studia/PRACA DYPLOMOWA/wilcox/{cityName}/IMGW.csv", observations);
            var forecasts = new Dictionary<string, IEnumerable<WeatherData>>();
            foreach (var api in _apis)
            {
                var forecast = GetForecastWeatherForApiByCity(api, 0, _weatherDataCount, cityName);
                forecasts.Add(api, forecast);
                AppendToXlsFile($"D:/Studia/PRACA DYPLOMOWA/wilcox/{cityName}/{api}.csv", forecast);

            }

            var result = new {WeatherDates = _observedDates, ObservedWeather = observations, ForecastWeather = forecasts};
            return Json(JsonConvert.SerializeObject(result), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetObservedDates()
        {
            CheckAndGetData();
            return Json(JsonConvert.SerializeObject(_observedDates), JsonRequestBehavior.AllowGet);
        }

        public void CheckAndGetData()
        {
            if (_weatherDataList == null || _weatherDataList.Count() != _weatherDataCount)
            {
                _weatherDataList = _weatherRepository.GetAll().ToList();
                _weatherDataCount = (int)_weatherRepository.GetWeatherDataCount();
                _missingIndexes = GetMissingDatesIndexes();
            }
        }

        public void AppendToXlsFile(string path, IEnumerable<WeatherData> data)
        {
            if (!System.IO.File.Exists(path))
            {
                System.IO.File.Create(path).Close();
            }

            using (var writer = new StreamWriter(path))
            using (var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csvWriter.Configuration.Delimiter = ";";
                foreach (var wp in _parameters)
                {
                    var dataParam = GetParameter(data, wp);
                    var dataJoined = string.Join(";", dataParam);
                    csvWriter.WriteField(dataJoined);
                    csvWriter.NextRecord();
                }
            }
        }
    }
}