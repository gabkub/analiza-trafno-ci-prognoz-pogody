﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using WeatherAccuracy.Models;

namespace WeatherAccuracy.DAL
{
    public class WeatherRepository : IWeatherRepository
    {
        private readonly MongoContext _context;

        public WeatherRepository(MongoContext context)
        {
            _context = context;
        }

        public IEnumerable<WeatherModel> GetAll()
        {
            var data = _context.Collection.Find(_ => true).ToList();
            data = data.OrderBy(x => x.DataPobrania).ToList();
            return data;
        }

        public WeatherModel Get(ObjectId id)
        {
            return _context.Collection.Find(document => document.Id == id).FirstOrDefault();
        }

        public void Update(WeatherModel model)
        {
            _context.Collection.ReplaceOne(document => document.Id == model.Id, model);
        }

        public void Add(WeatherModel model)
        {
            _context.Collection.InsertOne(model);
        }

        public void Delete(ObjectId id)
        {
            _context.Collection.FindOneAndDelete(document => document.Id == id);
        }

        public long GetWeatherDataCount()
        {
            return _context.Collection.CountDocuments(_ => true);
        }
    }
}