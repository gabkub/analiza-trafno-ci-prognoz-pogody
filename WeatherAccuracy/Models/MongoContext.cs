﻿using System.Configuration;
using MongoDB.Driver;
using WeatherAccuracy.Models;

namespace WeatherAccuracy
{
    public class MongoContext
    {
        private readonly IMongoClient _client;
        public IMongoDatabase Database;
        public IMongoCollection<WeatherModel> Collection;

        public MongoContext()
        {
            var MongoDatabaseName = ConfigurationManager.AppSettings["MongoDatabaseName"];
            var MongoCollectionName = ConfigurationManager.AppSettings["MongoCollectionName"];
            var MongoUsername = ConfigurationManager.AppSettings["MongoUsername"];
            var MongoPassword = ConfigurationManager.AppSettings["MongoPassword"];
            var connectionString = $"mongodb+srv://{MongoUsername}:{MongoPassword}" +
                                   $"@weather-5duzg.azure.mongodb.net/test?retryWrites=true&w=majority";
            
            _client = new MongoClient(connectionString);
            Database = _client.GetDatabase(MongoDatabaseName);
            Collection = Database.GetCollection<WeatherModel>(MongoCollectionName);
        }
    }       
}

