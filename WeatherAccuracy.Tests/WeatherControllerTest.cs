using WeatherAccuracy.Models;
using System.Collections.Generic;
using WeatherAccuracy.DAL;
// <copyright file="WeatherControllerTest.cs">Copyright ©  2020</copyright>
using System;
using System.Web.Mvc;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeatherAccuracy.Controllers;

namespace WeatherAccuracy.Controllers.Tests
{
    /// <summary>This class contains parameterized unit tests for WeatherController</summary>
    [PexClass(typeof(WeatherController))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class WeatherControllerTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        public WeatherController ConstructorTest()
        {
            WeatherController target = new WeatherController();
            return target;
            // TODO: add assertions to method WeatherControllerTest.ConstructorTest()
        }

        /// <summary>Test stub for .ctor(IWeatherRepository)</summary>
        [PexMethod]
        public WeatherController ConstructorTest01(IWeatherRepository weatherRepository)
        {
            WeatherController target = new WeatherController(weatherRepository);
            return target;
            // TODO: add assertions to method WeatherControllerTest.ConstructorTest01(IWeatherRepository)
        }

        /// <summary>Test stub for AccuracyData(String)</summary>
        [PexMethod]
        public JsonResult AccuracyDataTest([PexAssumeUnderTest] WeatherController target, string cityName)
        {
            JsonResult result = target.AccuracyData(cityName);
            return result;
            // TODO: add assertions to method WeatherControllerTest.AccuracyDataTest(WeatherController, String)
        }

        /// <summary>Test stub for CalculateSumOfSquared(IEnumerable`1&lt;Double&gt;)</summary>
        [PexMethod]
        public double CalculateSumOfSquaredTest([PexAssumeUnderTest] WeatherController target, IEnumerable<double> list)
        {
            double result = target.CalculateSumOfSquared(list);
            return result;
            // TODO: add assertions to method WeatherControllerTest.CalculateSumOfSquaredTest(WeatherController, IEnumerable`1<Double>)
        }

        /// <summary>Test stub for CalculateSumOfSquaredError(IEnumerable`1&lt;Double&gt;, IEnumerable`1&lt;Double&gt;)</summary>
        [PexMethod]
        public double CalculateSumOfSquaredErrorTest(
            [PexAssumeUnderTest] WeatherController target,
            IEnumerable<double> observedParameter,
            IEnumerable<double> forecastParameter
        )
        {
            double result = target.CalculateSumOfSquaredError(observedParameter, forecastParameter);
            return result;
            // TODO: add assertions to method WeatherControllerTest.CalculateSumOfSquaredErrorTest(WeatherController, IEnumerable`1<Double>, IEnumerable`1<Double>)
        }

        /// <summary>Test stub for CalculateU(Double, Double, Double)</summary>
        [PexMethod]
        public double CalculateUTest(
            [PexAssumeUnderTest] WeatherController target,
            double a,
            double b,
            double c
        )
        {
            double result = target.CalculateU(a, b, c);
            return result;
            // TODO: add assertions to method WeatherControllerTest.CalculateUTest(WeatherController, Double, Double, Double)
        }

        /// <summary>Test stub for CalculateUForApi(IEnumerable`1&lt;WeatherData&gt;, IEnumerable`1&lt;WeatherData&gt;)</summary>
        [PexMethod]
        public double CalculateUForApiTest(
            [PexAssumeUnderTest] WeatherController target,
            IEnumerable<WeatherData> observedWeather,
            IEnumerable<WeatherData> forecastWeather
        )
        {
            double result = target.CalculateUForApi(observedWeather, forecastWeather);
            return result;
            // TODO: add assertions to method WeatherControllerTest.CalculateUForApiTest(WeatherController, IEnumerable`1<WeatherData>, IEnumerable`1<WeatherData>)
        }

        /// <summary>Test stub for CheckAndGetData()</summary>
        [PexMethod]
        public void CheckAndGetDataTest([PexAssumeUnderTest] WeatherController target)
        {
            target.CheckAndGetData();
            // TODO: add assertions to method WeatherControllerTest.CheckAndGetDataTest(WeatherController)
        }

        /// <summary>Test stub for FillObservationDates()</summary>
        [PexMethod]
        public void FillObservationDatesTest([PexAssumeUnderTest] WeatherController target)
        {
            target.FillObservationDates();
            // TODO: add assertions to method WeatherControllerTest.FillObservationDatesTest(WeatherController)
        }

        /// <summary>Test stub for GetForecastWeatherForApi(String, Int32, Int32)</summary>
        [PexMethod]
        public IEnumerable<WeatherData> GetForecastWeatherForApiTest(
            [PexAssumeUnderTest] WeatherController target,
            string apiName,
            int indexStart,
            int indexEnd
        )
        {
            IEnumerable<WeatherData> result = target.GetForecastWeatherForApi(apiName, indexStart, indexEnd);
            return result;
            // TODO: add assertions to method WeatherControllerTest.GetForecastWeatherForApiTest(WeatherController, String, Int32, Int32)
        }

        /// <summary>Test stub for GetForecastWeatherForApiByCity(String, Int32, Int32, String)</summary>
        [PexMethod]
        public IEnumerable<WeatherData> GetForecastWeatherForApiByCityTest(
            [PexAssumeUnderTest] WeatherController target,
            string apiName,
            int indexStart,
            int indexEnd,
            string cityName
        )
        {
            IEnumerable<WeatherData> result
               = target.GetForecastWeatherForApiByCity(apiName, indexStart, indexEnd, cityName);
            return result;
            // TODO: add assertions to method WeatherControllerTest.GetForecastWeatherForApiByCityTest(WeatherController, String, Int32, Int32, String)
        }

        /// <summary>Test stub for GetIndexesToIgnore(Int32)</summary>
        [PexMethod]
        public List<int> GetIndexesToIgnoreTest([PexAssumeUnderTest] WeatherController target, int ind)
        {
            List<int> result = target.GetIndexesToIgnore(ind);
            return result;
            // TODO: add assertions to method WeatherControllerTest.GetIndexesToIgnoreTest(WeatherController, Int32)
        }

        /// <summary>Test stub for GetMissingDatesIndexes()</summary>
        [PexMethod]
        public List<int> GetMissingDatesIndexesTest([PexAssumeUnderTest] WeatherController target)
        {
            List<int> result = target.GetMissingDatesIndexes();
            return result;
            // TODO: add assertions to method WeatherControllerTest.GetMissingDatesIndexesTest(WeatherController)
        }

        /// <summary>Test stub for GetObservedDates()</summary>
        [PexMethod]
        public JsonResult GetObservedDatesTest([PexAssumeUnderTest] WeatherController target)
        {
            JsonResult result = target.GetObservedDates();
            return result;
            // TODO: add assertions to method WeatherControllerTest.GetObservedDatesTest(WeatherController)
        }

        /// <summary>Test stub for GetObservedWeather(Int32, Int32)</summary>
        [PexMethod]
        public IEnumerable<WeatherData> GetObservedWeatherTest(
            [PexAssumeUnderTest] WeatherController target,
            int indexStart,
            int indexEnd
        )
        {
            IEnumerable<WeatherData> result = target.GetObservedWeather(indexStart, indexEnd);
            return result;
            // TODO: add assertions to method WeatherControllerTest.GetObservedWeatherTest(WeatherController, Int32, Int32)
        }

        /// <summary>Test stub for GetObservedWeatherByCity(Int32, Int32, String)</summary>
        [PexMethod]
        public IEnumerable<WeatherData> GetObservedWeatherByCityTest(
            [PexAssumeUnderTest] WeatherController target,
            int indexStart,
            int indexEnd,
            string cityName
        )
        {
            IEnumerable<WeatherData> result = target.GetObservedWeatherByCity(indexStart, indexEnd, cityName);
            return result;
            // TODO: add assertions to method WeatherControllerTest.GetObservedWeatherByCityTest(WeatherController, Int32, Int32, String)
        }

        /// <summary>Test stub for GetSumOfSquared(IEnumerable`1&lt;WeatherData&gt;)</summary>
        [PexMethod]
        public double GetSumOfSquaredTest([PexAssumeUnderTest] WeatherController target, IEnumerable<WeatherData> weather)
        {
            double result = target.GetSumOfSquared(weather);
            return result;
            // TODO: add assertions to method WeatherControllerTest.GetSumOfSquaredTest(WeatherController, IEnumerable`1<WeatherData>)
        }

        /// <summary>Test stub for GetSumOfSquaredErrors(IEnumerable`1&lt;WeatherData&gt;, IEnumerable`1&lt;WeatherData&gt;)</summary>
        [PexMethod]
        public double GetSumOfSquaredErrorsTest(
            [PexAssumeUnderTest] WeatherController target,
            IEnumerable<WeatherData> observedWeather,
            IEnumerable<WeatherData> forecastWeather
        )
        {
            double result = target.GetSumOfSquaredErrors(observedWeather, forecastWeather);
            return result;
            // TODO: add assertions to method WeatherControllerTest.GetSumOfSquaredErrorsTest(WeatherController, IEnumerable`1<WeatherData>, IEnumerable`1<WeatherData>)
        }

        /// <summary>Test stub for GetWeatherJson(String)</summary>
        [PexMethod]
        public JsonResult GetWeatherJsonTest([PexAssumeUnderTest] WeatherController target, string cityName)
        {
            JsonResult result = target.GetWeatherJson(cityName);
            return result;
            // TODO: add assertions to method WeatherControllerTest.GetWeatherJsonTest(WeatherController, String)
        }

        /// <summary>Test stub for Ranking()</summary>
        [PexMethod]
        public ActionResult RankingTest([PexAssumeUnderTest] WeatherController target)
        {
            ActionResult result = target.Ranking();
            return result;
            // TODO: add assertions to method WeatherControllerTest.RankingTest(WeatherController)
        }

        /// <summary>Test stub for WeatherData()</summary>
        [PexMethod]
        public ActionResult WeatherDataTest([PexAssumeUnderTest] WeatherController target)
        {
            ActionResult result = target.WeatherData();
            return result;
            // TODO: add assertions to method WeatherControllerTest.WeatherDataTest(WeatherController)
        }
    }
}
